import { Platform } from 'react-native';
const srcImage = 'srcImage';
const dstImage = 'dstImage';
const toBackground = {
    inputImage: srcImage,
    inputBackgroundImage: dstImage
};
const convertMap = Platform.select({
    android: {},
    ios: {
        IosCIMaskedVariableBlur: {
            inputImage: srcImage,
            inputMask: dstImage
        },
        IosCIDisplacementDistortion: {
            inputImage: srcImage,
            inputDisplacementImage: dstImage
        },
        IosCIColorMap: {
            inputImage: srcImage,
            inputGradientImage: dstImage
        },
        IosCIGlassDistortion: {
            inputImage: srcImage,
            inputTexture: dstImage
        },
        IosCIShadedMaterial: {
            inputImage: srcImage,
            inputShadingImage: dstImage
        },
        IosCIMix: toBackground,
        IosCIAdditionCompositing: toBackground,
        IosCIColorBlendMode: toBackground,
        IosCIColorBurnBlendMode: toBackground,
        IosCIColorDodgeBlendMode: toBackground,
        IosCIDarkenBlendMode: toBackground,
        IosCIDifferenceBlendMode: toBackground,
        IosCIDivideBlendMode: toBackground,
        IosCIExclusionBlendMode: toBackground,
        IosCIHardLightBlendMode: toBackground,
        IosCIHueBlendMode: toBackground,
        IosCILightenBlendMode: toBackground,
        IosCILinearBurnBlendMode: toBackground,
        IosCILinearDodgeBlendMode: toBackground,
        IosCILuminosityBlendMode: toBackground,
        IosCIMaximumCompositing: toBackground,
        IosCIMinimumCompositing: toBackground,
        IosCIMultiplyBlendMode: toBackground,
        IosCIMultiplyCompositing: toBackground,
        IosCIOverlayBlendMode: toBackground,
        IosCIPinLightBlendMode: toBackground,
        IosCISaturationBlendMode: toBackground,
        IosCIScreenBlendMode: toBackground,
        IosCISoftLightBlendMode: toBackground,
        IosCISourceAtopCompositing: toBackground,
        IosCISourceInCompositing: toBackground,
        IosCISourceOutCompositing: toBackground,
        IosCISourceOverCompositing: toBackground,
        IosCISubtractBlendMode: toBackground
    }
});
export const convertImageName = (filterName, normalImageName) => (
// tslint:disable-next-line: no-non-null-assertion
convertMap[filterName] && convertMap[filterName][normalImageName]
    || normalImageName);
//# sourceMappingURL=convert-image-name.js.map